﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class BooksIssue
    {
            public int IssueID{get;set;}
            public int BookID{get;set;}
            public string StudentID{get;set;}
            public DateTime? IssueDate{get;set;}
            public DateTime? ReturnDate{get;set;}
            public DateTime? ActualReturnDate{get;set;}
            public string StudentName{get;set;}
            public string StudentMobile{get;set;}
            public string BookName { get; set; }
            public string BookAuthor { get; set; }
            public int BookStock { get; set; }
            public int CurrentStock { get; set; }

    }
}