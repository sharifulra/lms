﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.Models
{
    public class Books
    {
      public int BookID { get; set; }
      public string BookNo { get; set; }
      public string BookName { get; set; }
      public string BookAuthor { get; set; }
        public string BookDescription { get; set; }
        public string BookType { get; set; }
        public int BookStock { get; set; }
        public int CurrentStock { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}