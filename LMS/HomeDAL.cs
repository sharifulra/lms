﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LMS.DataManager;
using LMS.Models;

namespace LMS
{
    public class HomeDAL
    {

        private DataAccessManager accessManager = new DataAccessManager();



        public bool SaveBooks(Books aMaster)
        {
            bool result = true;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.Lms);

                List<SqlParameter> mParameters = new List<SqlParameter>();
                mParameters.Add(new SqlParameter("@BookName", aMaster.BookName));
                mParameters.Add(new SqlParameter("@bookAuthor", aMaster.BookAuthor));
                mParameters.Add(new SqlParameter("@bookDescription", aMaster.BookDescription));
                mParameters.Add(new SqlParameter("@bookType", aMaster.BookType));
                mParameters.Add(new SqlParameter("@stock", aMaster.BookStock));
                mParameters.Add(new SqlParameter("@isActive", aMaster.IsActive));
                result = accessManager.SaveData("sp_AddBooks", mParameters);

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public List<Books> GetBooks()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.Lms);
                List<Books> aBookses = new List<Books>();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBooksLibrarian");
                while (dr.Read())
                {
                    Books aBooks = new Books();
                    aBooks.BookID = (int) dr["BookID"];
                    aBooks.BookName = dr["BookName"].ToString();
                    aBooks.BookAuthor = dr["BookAuthor"].ToString();
                    aBooks.BookDescription = dr["BookDescription"].ToString();
                    aBooks.BookType = dr["BookType"].ToString();
                    aBooks.BookStock =(int) dr["BookStock"];
                    aBooks.CurrentStock = (int) dr["CurrentStock"];
                    aBookses.Add(aBooks);
                }
            

                return aBookses;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public Books GetBookDetails(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.Lms);
                Books aBooks = new Books();
                List<SqlParameter> aList = new List<SqlParameter>();
                aList.Add(new SqlParameter("@id", id));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBookDetails", aList);
                while (dr.Read())
                {
                    
                    aBooks.BookID = (int)dr["BookID"];
                    aBooks.BookName = dr["BookName"].ToString();
                    aBooks.BookAuthor = dr["BookAuthor"].ToString();
                    aBooks.BookDescription = dr["BookDescription"].ToString();
                    aBooks.BookType = dr["BookType"].ToString();
                    aBooks.BookStock = (int)dr["BookStock"];
                    aBooks.CurrentStock = (int)dr["CurrentStock"];
                    aBooks.IsActive = (bool)dr["IsActive"];
                   
                }


                return aBooks;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveIssue(BooksIssue aIssue)
        {
            try
            {

                bool result = false;
                accessManager.SqlConnectionOpen(DataBase.Lms);
                List<SqlParameter> aPa = new List<SqlParameter>();
                aPa.Add(new SqlParameter("@BookID", aIssue.BookID));
                aPa.Add(new SqlParameter("@StudentID", aIssue.StudentID));
                aPa.Add(new SqlParameter("@IssueDate", aIssue.IssueDate));
                aPa.Add(new SqlParameter("@ReturnDate", aIssue.ReturnDate));
                aPa.Add(new SqlParameter("@StudentMobile", aIssue.StudentMobile));
                aPa.Add(new SqlParameter("@StudentName", aIssue.StudentName));
                result = accessManager.SaveData("sp_saveIssue", aPa);
                return result;
            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<BooksIssue> GetIssueList( int id = 0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.Lms);

                List<SqlParameter> aPar = new List<SqlParameter>();
                aPar.Add(new SqlParameter("@id" , id));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_IssueBookList",aPar);
                List<BooksIssue> aList = new List<BooksIssue>();
                while (dr.Read())
                {
                    BooksIssue aBook = new BooksIssue();
                    aBook.BookID = (int)dr["BookID"];
                    aBook.CurrentStock = (int)dr["CurrentStock"];
                    aBook.BookStock = (int)dr["BookStock"];
                    aBook.IssueID = (int)dr["IssueID"];
                    aBook.ReturnDate = dr["BookStock"] as DateTime?;
                    aBook.IssueDate = dr["IssueDate"] as DateTime?;
                    aBook.StudentID = dr["StudentID"].ToString();
                    aBook.StudentName = dr["StudentName"].ToString();
                    aBook.BookAuthor = dr["BookAuthor"].ToString();
                    aBook.BookName = dr["BookName"].ToString();
                    aBook.StudentMobile = dr["StudentMobile"].ToString();
                    aList.Add(aBook);
                }

                return aList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveReceive(BooksIssue aIssue)
        {
            try
            {
                bool result = false;
                accessManager.SqlConnectionOpen(DataBase.Lms);
                List<SqlParameter> aPa = new List<SqlParameter>();
                aPa.Add(new SqlParameter("@IssueID", aIssue.IssueID));
                aPa.Add(new SqlParameter("@ActualReturnDate", aIssue.ActualReturnDate));
                result = accessManager.SaveData("sp_receive_Book", aPa);
                return result;

            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

    }
}