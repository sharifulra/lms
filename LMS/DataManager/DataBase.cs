﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMS.DataManager
{
    public class DataBase
    {
        public static string ControlPanel = @"ControlPanelDB";
        public static string FarmManagement = @"FarmManagementDB";
        public static string SupplyChain = @"SupplyChainDB";
        public static string CommercialHatcheryDB = @"CommercialHatcheryDB";
        public static string Lms = @"LMSJU";
    }
}