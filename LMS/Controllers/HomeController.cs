﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LMS.Models;

namespace LMS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }


        public ActionResult CheckLogin()
        {
            return Json(true);
        }



        public ActionResult AddBooks()
        {
            return View();
        }

        public ActionResult BookList()
        {
            HomeDAL aDal = new HomeDAL();
            return View(aDal.GetBooks());
        }


        [HttpPost]
        public ActionResult Save(Books aBooks)
        {
            var result = false;
            HomeDAL aDal = new HomeDAL();
            result = aDal.SaveBooks(aBooks);

            return Json(result, JsonRequestBehavior.AllowGet);


        }


        public ActionResult BookIssue()
        {
            HomeDAL aDal = new HomeDAL();
            return View(aDal.GetBooks());
        }

        public ActionResult GetBookDetails(int id)
        {
            HomeDAL aDal = new HomeDAL();
            Books aBook = aDal.GetBookDetails(id);

            return Json(aBook, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveIssue(BooksIssue abook)
        {
            HomeDAL aDal = new HomeDAL();
            bool result = aDal.SaveIssue(abook);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BookReturn()
        {
            HomeDAL aDal = new HomeDAL();
            List<BooksIssue> aList = aDal.GetIssueList();
            return View(aList);
        }

        public ActionResult SaveReceive(BooksIssue aRec)
        {
            HomeDAL aDal = new HomeDAL();
            bool result = aDal.SaveReceive(aRec);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBookDetailsIssue(int id)
        {
            HomeDAL aDal = new HomeDAL();
            List<BooksIssue> aList = aDal.GetIssueList(id);
            BooksIssue aIssue = aList[0];
            return Json(aIssue, JsonRequestBehavior.AllowGet);
        }

        








    }
}